package com.ink_global.apps.weatherfeed;

import java.net.URISyntaxException;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ink_global.apps.weatherfeed.constants.Constants;
import com.ink_global.apps.weatherfeed.model.WeatherInfo;
import com.ink_global.apps.weatherfeed.model.loader.ModelLoader;
import com.ink_global.apps.weatherfeed.net.WeatherHttpClient;
import com.ink_global.apps.weatherfeed.parser.WeatherFeedParser;
import com.squareup.picasso.Picasso;

public class WeatherPageFeedFragment extends Fragment {
	private static final int DOWNLOADING_DATA = 0;
	private WeatherDataLoader mLoader;
	private String cityName;
	private TextView titleTextView, geopointsTextView, humidityTextView,
			descriptionTextView, temperatureTextView, tempartureRangeTextVeiw,
			pressureTextView;
	private ImageView weatherImageView;
	private View rootView;
	private static final int MAIN_LOADER = 1001;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle dataBundle = getArguments();
		if (dataBundle != null) {
			cityName = dataBundle
					.getString(Constants.WEATHER_FRAGMENT_BUNDLE_KEY_CITYNAME);

		}
		rootView = inflater.inflate(R.layout.weather_info_layout, null);
		initViews();
		mLoader = new WeatherDataLoader();
		getActivity().getSupportLoaderManager().initLoader(MAIN_LOADER, null,
				mLoader);
		return rootView;
	}

	public void initViews() {

		titleTextView = (TextView) rootView.findViewById(R.id.title);
		titleTextView.setText(cityName);
		geopointsTextView = (TextView) rootView.findViewById(R.id.geopoints);
		humidityTextView = (TextView) rootView.findViewById(R.id.humidity);
		temperatureTextView = (TextView) rootView
				.findViewById(R.id.temperature);
		tempartureRangeTextVeiw = (TextView) rootView
				.findViewById(R.id.temperature_range);
		pressureTextView = (TextView) rootView.findViewById(R.id.pressure);
		descriptionTextView = (TextView) rootView
				.findViewById(R.id.description);
		weatherImageView = (ImageView) rootView
				.findViewById(R.id.weather_condition);

	}
	public static WeatherPageFeedFragment getInstance(Bundle bundle) {
		WeatherPageFeedFragment weatherFragment = new WeatherPageFeedFragment();
		weatherFragment.setArguments(bundle);
		return weatherFragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		try {
			downloadData();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {
				case DOWNLOADING_DATA :
					getActivity().getSupportLoaderManager().restartLoader(
							MAIN_LOADER, null, mLoader);

					break;

			}
		}
	};

	/**
	 * tried to use an elegant approach to building the url with params..had an
	 * exception..had to move to no time to fix not too bad ..only 4 feeds
	 */
	private void downloadData() throws URISyntaxException {
		// Downloading data using JSON Client
		WeatherHttpClient client = new WeatherHttpClient(getActivity(),
				handler, DOWNLOADING_DATA, WeatherFeedParser.class);
		String destinationUrl = Constants.BASE_URL
				+ Constants.PATH_TO_WEATHER_DATA + "?" + "q=" + cityName
				+ ",uk";
		client.execute(destinationUrl);
		// show progress-bar
		// UiUtils.showProgressBar(getActivity(), getString(R.string.loading),
		// this);
	}
	private class WeatherDataLoader
			implements
				LoaderManager.LoaderCallbacks<List<WeatherInfo>> {
		@Override
		public Loader<List<WeatherInfo>> onCreateLoader(int id, Bundle args) {
			return new ModelLoader<WeatherInfo>(getActivity(),
					WeatherInfo.class, new Select().from(WeatherInfo.class)
							.where("CityName = ?", cityName), false);
		}

		@Override
		public void onLoaderReset(Loader<List<WeatherInfo>> loader) {
			mLoader.onLoaderReset(loader);
		}

		@Override
		public void onLoadFinished(Loader<List<WeatherInfo>> arg0,
				List<WeatherInfo> cachedWeatherInfo) {
			if (!cachedWeatherInfo.isEmpty()) {

				WeatherInfo weatherInfo = cachedWeatherInfo.get(0);
				pressureTextView.setText(weatherInfo.getPressure());
				geopointsTextView
						.setText(String.format("%s/%s",
								weatherInfo.getLongitude(),
								weatherInfo.getLattitude()));
				humidityTextView.setText(weatherInfo.getHumidity());
				tempartureRangeTextVeiw.setText(String.format("%s/%s",
						weatherInfo.getTemp_max(), weatherInfo.getTemp_min()));
				temperatureTextView.setText(weatherInfo.getTemperature());
				descriptionTextView.setText(weatherInfo.getDescription());
				Picasso.with(getActivity()).load(weatherInfo.getIconUrl())
						.placeholder(R.drawable.placeholder)
						.error(R.drawable.error).into(weatherImageView);

			}

		}
	}

}
