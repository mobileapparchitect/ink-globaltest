package com.ink_global.apps.weatherfeed.constants;

import org.apache.http.message.BasicNameValuePair;

public class Constants {
	public static final String BASE_URL = "http://api.openweathermap.org";
	public static final String PATH_TO_WEATHER_DATA = "/data/2.5/weather";
	public static final BasicNameValuePair paramPair = new BasicNameValuePair(
			"q", "London,uk");
	public static final String WEATHER_FRAGMENT_BUNDLE_KEY_CITYNAME = "cityName";
}
