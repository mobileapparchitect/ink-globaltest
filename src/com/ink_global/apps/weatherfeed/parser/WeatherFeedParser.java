package com.ink_global.apps.weatherfeed.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ink_global.apps.weatherfeed.model.WeatherInfo;
import com.ink_global.apps.weatherfeed.net.IBaseParser;

public class WeatherFeedParser implements IBaseParser {
	private JSONObject jsonObject;
	@Override
	public void init(String str) {
		try {
			jsonObject = new JSONObject(str);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public boolean isParsable() {
		// TODO Auto-generated method stub
		return false;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ink_global.apps.weatherfeed.net.IBaseParser#parse() acttually
	 * could have used GSON but...
	 */
	@Override
	public Object parse() {
		WeatherInfo info = new WeatherInfo();
		if (this.jsonObject != null) {

			try {
				if (jsonObject.has("coord")) {

					JSONObject coordObject = jsonObject.getJSONObject("coord");
					info.setLattitude(coordObject.getString("lat"));
					info.setLongitude(coordObject.getString("lon"));
				}
				if (jsonObject.has("sys")) {
					JSONObject sysObject = jsonObject.getJSONObject("sys");
					info.setMessage(sysObject.getString("message"));
					info.setCountry(sysObject.getString("country"));
					info.setSunrise(sysObject.getLong("sunrise"));
					info.setSunset(sysObject.getLong("sunset"));
				}
				if (jsonObject.has("weather")) {

					JSONArray weatherArray = jsonObject.getJSONArray("weather");
					JSONObject weatherObject = weatherArray.getJSONObject(0);
					info.setMain(weatherObject.getString("main"));
					info.setDescription(weatherObject.getString("description"));
					info.setIconUrl("http://openweathermap.org/img/w/"
							+ weatherObject.getString("icon") + ".png");

				}
				if (jsonObject.has("main")) {
					JSONObject mainObject = jsonObject.getJSONObject("main");
					info.setTemperature(mainObject.getString("temp"));
					info.setTemp_max(mainObject.getString("temp_max"));
					info.setTemp_min(mainObject.getString("temp_min"));
					info.setHumidity(mainObject.getString("humidity"));
					info.setPressure(mainObject.getString("pressure"));
				}
				if (jsonObject.has("wind")) {
					JSONObject windObject = jsonObject.getJSONObject("wind");
					info.setWind_speed(windObject.getString("speed"));
					info.setWind_deg(windObject.getString("deg"));

				}
				if (jsonObject.has("name")) {
					info.setCityName(jsonObject.getString("name"));

				}
				if (jsonObject.has("dt")) {
					info.setDt(jsonObject.getString("dt"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			info.save();
		}
		return null;
	}
}
