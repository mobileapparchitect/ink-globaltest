package com.ink_global.apps.weatherfeed.net;

public interface IBaseProvider {
	
	public abstract void init();
	
	public abstract void init(Object object);

	public abstract Object getPayload();

	public abstract String getContentType();
	
	public abstract String getAcceptType();
	
	public abstract String getRequestMethod();
}
