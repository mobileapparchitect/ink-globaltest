package com.ink_global.apps.weatherfeed.net;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.ink_global.apps.weatherfeed.R;
import com.ink_global.apps.weatherfeed.parser.WeatherFeedParser;
import com.ink_global.apps.weatherfeed.utils.Utils;

public class WeatherHttpClient extends AsyncTask<String, Void, Response> {
	private Context mContext;

	private Class<?> parserClass;

	private Handler handler;

	private int messageCode;

	private IBaseProvider provider = null;

	private static final String TAG = WeatherHttpClient.class.getSimpleName();

	private Response response = new Response();

	public WeatherHttpClient(Context context, Handler handler, int messageCode,
			Class<?> parserClass) {
		this.handler = handler;
		this.messageCode = messageCode;
		this.mContext = context;
		this.parserClass = parserClass;
	}

	public WeatherHttpClient(Context context, Handler handler, int messageCode,
			Class<?> parserClass, IBaseProvider provider) {

		this(context, handler, messageCode, parserClass);

		this.provider = provider;
	}

	/**
	 * Method reads response String InputStream using the
	 * BufferedReader.readLine() method.  iterate until the BufferedReader
	 * return null which means there's no more data to read. Each line will
	 * appended to a StringBuilder and returned as String.
	 */
	private static String convertStreamToString(InputStream is) {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(is, "utf-8"));

			StringBuilder sb = new StringBuilder();

			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return sb.toString();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		return null;
	}

	private int statusCode = -1;

	private int getStatusCode() {
		return statusCode;
	}

	/**
	 * This method downloads data using HttpGet. Currently supports basic
	 * validation for status code.
	 * 
	 */
	public Response processDownloadRequest(String requestUrl) {

		URL url = null;

		if (!Utils.isNetworkAvailable(mContext)) {
			createErrorResponse(R.string.network_not_available);
		} else {
			try {

				url = new URL(requestUrl);
				String result = downloadData(url);
				IBaseParser parser = new WeatherFeedParser();
				parser.init(result);
				parser.parse();
				response.setData(parser.parse());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return response;
	}

	private String downloadData(URL url) {
		String result = null;

		HttpURLConnection urlConnection = null;

		try {
			urlConnection = (HttpURLConnection) url.openConnection();

			// default to get
			if (null != provider) {
				urlConnection.setRequestMethod(provider.getRequestMethod());
				urlConnection.setRequestProperty("Content-Type",
						provider.getContentType());
				urlConnection.setRequestProperty("Accept",
						provider.getAcceptType());

				// adding json post pay-load
				OutputStreamWriter os = new OutputStreamWriter(
						urlConnection.getOutputStream(), "UTF-8");
				os.write(provider.getPayload().toString());
				os.flush();
				os.close();

				Log.d(TAG, "=== Request payload: " + provider.getPayload());
			} else {
				// well for this purpose not processing get requests
			}

			// getting response status code
			statusCode = urlConnection.getResponseCode();

			if (statusCode == HttpURLConnection.HTTP_OK) {
				InputStream instream = new BufferedInputStream(
						urlConnection.getInputStream());

				// converting stream to string
				result = convertStreamToString(instream);

				Log.d(TAG, "=== Success Response: " + result);

				// input stream closed
				instream.close();

			} else {
				InputStream instream = new BufferedInputStream(
						urlConnection.getErrorStream());

				// converting stream to string
				String error = convertStreamToString(instream);

				Log.d(TAG, "=== Error Response: " + error);

				// input stream closed
				instream.close();

				createErrorResponse(url, statusCode, error);
			}

		} catch (IOException e) {
			createErrorResponse(R.string.server_exception);
			e.printStackTrace();
		} finally {
			if (null != urlConnection) {
				urlConnection.disconnect();
			}
		}

		return result;
	}

	public void createErrorResponse(URL url, int httpErroCode, String errorMsg) {
		com.ink_global.apps.weatherfeed.model.Error error = new com.ink_global.apps.weatherfeed.model.Error();
		JSONObject object;
		try {
			object = new JSONObject(errorMsg);
			if (object != null) {
				error.setDescription(object.optString("error"));

				final String errorCode = object.optString("error_description");
				if (Utils.isNumeric(errorCode)) {
					error.setErrorCode(Integer.parseInt(errorCode));
				}

				error.setErrorMessage(ErrorCode.getGenericHttpError(mContext,
						httpErroCode, errorCode));
				error.setHttpCode(httpErroCode);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		response.setCode(httpErroCode);
		response.setData(null);
		response.setErrorResponse(error);
	}

	public void createErrorResponse(int stringId) {
		response.setCode(getStatusCode());
		response.setMessage(mContext.getResources().getString(stringId));
		response.setData(null);

	}

	@Override
	public void onPreExecute() {
		// TODO validation before downloading
	}

	@Override
	protected Response doInBackground(String... urls) {
		return processDownloadRequest(urls[0]);
	}

	@Override
	protected void onPostExecute(Response response) {
		Message msgObj = handler.obtainMessage();
		msgObj.what = messageCode;
		msgObj.obj = response;
		handler.sendMessage(msgObj);
	}
}