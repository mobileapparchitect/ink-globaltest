package com.ink_global.apps.weatherfeed.net;

import com.ink_global.apps.weatherfeed.model.Error;



public class Response {
	
	private Object data;

	private int code;

	private String message;
	
	private Error errorResponse;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public Error getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(com.ink_global.apps.weatherfeed.model.Error error) {
		this.errorResponse = error;
	}

	@Override
	public String toString() {
		return "[code=" + code + " , message=" + message + "]";
	}
}
