package com.ink_global.apps.weatherfeed.net;

public interface IBaseParser {
	void init(String str);

	boolean isParsable();

	Object parse();
}
