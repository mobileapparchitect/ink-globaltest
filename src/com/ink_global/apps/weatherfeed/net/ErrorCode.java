package com.ink_global.apps.weatherfeed.net;

import android.content.Context;

import com.ink_global.apps.weatherfeed.R;

public class ErrorCode {
	public String getErrorCode(int httpErrorCode, int serverErrorCode) {
		return "";
	}

	public String getErrorCode(int errorCode) {
		return "";
	}

	/** generic http messages */
	public static String getGenericHttpError(Context context,
			int httpErrorCode, String errorcode) {
		String mesasge = "";
		final String appName = context.getString(R.string.app_name);

		if (httpErrorCode == 500) {
			mesasge = String.format(context.getString(R.string.http_500),
					appName);
		} else if (httpErrorCode == 400) {
			mesasge = String.format(context.getString(R.string.http_400),
					appName);
		}
		return mesasge;
	}

	
}
