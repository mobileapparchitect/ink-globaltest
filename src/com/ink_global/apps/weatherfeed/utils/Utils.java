package com.ink_global.apps.weatherfeed.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	//checks if an string is a valid numeric
		public static boolean isNumeric(String string) {
			if (string == null) {
				return false;
			}
			int sz = string.length();
			for (int i = 0; i < sz; i++) {
				if (Character.isDigit(string.charAt(i)) == false) {
					return false;
				}
			}
			return true;
		}
	
}
