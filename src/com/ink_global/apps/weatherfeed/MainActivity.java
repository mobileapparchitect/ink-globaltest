package com.ink_global.apps.weatherfeed;

import com.ink_global.apps.weatherfeed.constants.Constants;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class MainActivity extends FragmentActivity
		implements
			ActionBar.TabListener {

	private WeatherFeedPageAdapter weatherPages;
	private static final int NUMBER_OF_CITIES = 4;

	private ViewPager mViewPager;
	private static String[] namesOfCities = {"London", "Luton", "Manchester",
			"Birmingham"};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		weatherPages = new WeatherFeedPageAdapter(getSupportFragmentManager());

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(weatherPages);
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		for (int i = 0; i < weatherPages.getCount(); i++) {
			actionBar
					.addTab(actionBar.newTab()
							.setText(weatherPages.getPageTitle(i))
							.setTabListener(this));
		}
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	public static class WeatherFeedPageAdapter extends FragmentPagerAdapter {

		public WeatherFeedPageAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Bundle bundle = new Bundle();
			bundle.putString(Constants.WEATHER_FRAGMENT_BUNDLE_KEY_CITYNAME,
					namesOfCities[(position % NUMBER_OF_CITIES)]);
			switch (position) {
				case 0 :
					return WeatherPageFeedFragment.getInstance(bundle);
				case 1 :
					return WeatherPageFeedFragment.getInstance(bundle);
				case 2 :
					return WeatherPageFeedFragment.getInstance(bundle);
				case 3 :
					return WeatherPageFeedFragment.getInstance(bundle);

			}
			return null;
		}

		@Override
		public int getCount() {
			return NUMBER_OF_CITIES;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "City: " + namesOfCities[(position % NUMBER_OF_CITIES)];
		}
	}

}
