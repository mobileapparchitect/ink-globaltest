package com.ink_global.apps.weatherfeed.model;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "WeatherInfo")
public class WeatherInfo extends com.activeandroid.Model {

	@Column(name = "Longitude")
	private String longitude;

	@Column(name = "Lattitude")
	private String lattitude;

	@Column(name = "DT")
	private String dt;

	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	@Column(name = "Message")
	private String message;

	@Column(name = "Country")
	private String country;

	@Column(name = "Sunrise")
	private long sunrise;

	@Column(name = "Sunset")
	private long sunset;

	@Column(name = "Main")
	private String main;
	@Column(name = "Description")
	private String description;

	@Column(name = "IconUrl")
	private String iconUrl;

	@Column(name = "Temp")
	private String temperature;

	@Column(name = "Pressure")
	private String pressure;

	@Column(name = "Humidity")
	private String humidity;

	@Column(name = "Temp_Min")
	private String temp_min;

	@Column(name = "Temp_Max")
	private String temp_max;
	
	@Column(name = "Wind_Speed")
	private String wind_speed;
	
	@Column(name = "Wind_Deg")
	private String wind_deg;
	
	@Column(name = "Rain_Time")
	private String rainTime;
	
	@Column(name = "CityName")
	private String cityName;
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLattitude() {
		return lattitude;
	}
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public long getSunrise() {
		return sunrise;
	}
	public void setSunrise(long sunrise) {
		this.sunrise = sunrise;
	}
	public long getSunset() {
		return sunset;
	}
	public void setSunset(long sunset) {
		this.sunset = sunset;
	}
	public String getMain() {
		return main;
	}
	public void setMain(String main) {
		this.main = main;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getPressure() {
		return pressure;
	}
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getTemp_min() {
		return temp_min;
	}
	public void setTemp_min(String temp_min) {
		this.temp_min = temp_min;
	}
	public String getTemp_max() {
		return temp_max;
	}
	public void setTemp_max(String temp_max) {
		this.temp_max = temp_max;
	}
	public String getWind_speed() {
		return wind_speed;
	}
	public void setWind_speed(String wind_speed) {
		this.wind_speed = wind_speed;
	}
	public String getWind_deg() {
		return wind_deg;
	}
	public void setWind_deg(String wind_deg) {
		this.wind_deg = wind_deg;
	}
	public String getRainTime() {
		return rainTime;
	}
	public void setRainTime(String rainTime) {
		this.rainTime = rainTime;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
