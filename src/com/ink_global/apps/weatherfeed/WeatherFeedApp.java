package com.ink_global.apps.weatherfeed;

import android.content.Context;

import com.activeandroid.ActiveAndroid;

public class WeatherFeedApp extends com.activeandroid.app.Application {
	private static Context mContext;
	private static final String TAG = WeatherFeedApp.class.getSimpleName();

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = this;
		ActiveAndroid.initialize(this);

	}

	public static Context getAppContext() {
		return WeatherFeedApp.mContext;
	}

	public static WeatherFeedApp getApplication(Context context) {
		return (WeatherFeedApp) context.getApplicationContext();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		ActiveAndroid.dispose();

	}

}